package com.kirkchan.poc.xyz;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.Map;

import com.kirkchan.poc.xyz.api.queue.JsonMapper;
import com.kirkchan.poc.xyz.api.queue.AbstractQueue;
import com.kirkchan.poc.xyz.api.queue.ShipmentQueue;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * Tests for the mechanics of the AbstractQueue classes
 */
@SpringBootTest
class QueueTests {

    @Mock
    private RestTemplate backendCaller;

    @InjectMocks
    private AbstractQueue shipmentQueue = new ShipmentQueue(backendCaller);

    /**
     * Every queue needs to return a key so that the requestor can find their
     * respective request after waiting.
     */
    @Test
    public void testForQueuedRequest() {
        String request = "109347263,123456891";

        ResponseEntity<JsonMapper> response = this.shipmentQueue.queue(constructUrl(request));

        assertEquals("PROCESSING", response.getBody().getValues().get("status"));
        assertEquals(HttpStatus.ACCEPTED.value(), response.getStatusCode().value());
    }

    /**
     * Five requests should return successful responses
     */
    @Test
    public void testFiveSuccessfulRequests() {

        String[] keys = this.processFiveRequests();

        ResponseEntity<JsonMapper> resp1 = this.shipmentQueue.pullResponse(keys[0]);
        ResponseEntity<JsonMapper> resp2 = this.shipmentQueue.pullResponse(keys[1]);
        ResponseEntity<JsonMapper> resp3 = this.shipmentQueue.pullResponse(keys[2]);
        ResponseEntity<JsonMapper> resp4 = this.shipmentQueue.pullResponse(keys[3]);
        ResponseEntity<JsonMapper> resp5 = this.shipmentQueue.pullResponse(keys[4]);

        /**
         * Tests per respective request
         *  1. Correct responses
         *  2. Response removed after first successful return of response
         */
        Map<String, Object> respBody1 = resp1.getBody().getValues();
        ResponseEntity<JsonMapper> temp1 = this.shipmentQueue.pullResponse(keys[0]);
        assertEquals(HttpStatus.OK.value(), resp1.getStatusCode().value());
        assertEquals("val1", respBody1.get("111"));
        assertEquals(HttpStatus.NOT_FOUND.value(), temp1.getStatusCode().value());

        Map<String, Object> respBody2 = resp2.getBody().getValues();
        ResponseEntity<JsonMapper> temp2 = this.shipmentQueue.pullResponse(keys[1]);
        assertEquals(HttpStatus.OK.value(), resp2.getStatusCode().value());
        assertEquals("val2", respBody2.get("222"));
        assertEquals(HttpStatus.NOT_FOUND.value(), temp2.getStatusCode().value());

        Map<String, Object> respBody3 = resp3.getBody().getValues();
        ResponseEntity<JsonMapper> temp3 = this.shipmentQueue.pullResponse(keys[2]);
        assertEquals(HttpStatus.OK.value(), resp3.getStatusCode().value());
        assertEquals("val3", respBody3.get("333"));
        assertEquals(HttpStatus.NOT_FOUND.value(), temp3.getStatusCode().value());

        Map<String, Object> respBody4 = resp4.getBody().getValues();
        ResponseEntity<JsonMapper> temp4 = this.shipmentQueue.pullResponse(keys[3]);
        assertEquals(HttpStatus.OK.value(), resp4.getStatusCode().value());
        assertEquals("val4", respBody4.get("444"));
        assertEquals(HttpStatus.NOT_FOUND.value(), temp4.getStatusCode().value());

        Map<String, Object> respBody5 = resp5.getBody().getValues();
        ResponseEntity<JsonMapper> temp5 = this.shipmentQueue.pullResponse(keys[4]);
        assertEquals(HttpStatus.OK.value(), resp5.getStatusCode().value());
        assertEquals("val5", respBody5.get("555"));
        assertEquals(HttpStatus.NOT_FOUND.value(), temp5.getStatusCode().value());
    }

    /**
     * Helper method to setup five requests processed and returns the five keys to pull from queue.
     */
    private String[] processFiveRequests() {
        
        // Loads of setup
        String req1 = "111";
        String req2 = "222";
        String req3 = "333";
        String req4 = "444";
        String req5 = "555";

        String bigReq = "111,222,333,444,555";

        JsonMapper responseBody = new JsonMapper();
        responseBody.setValue("111", "val1");
        responseBody.setValue("222", "val2");
        responseBody.setValue("333", "val3");
        responseBody.setValue("444", "val4");
        responseBody.setValue("555", "val5");

        Mockito.when(backendCaller.getForEntity(constructUrl(bigReq), JsonMapper.class)).thenReturn(new ResponseEntity<JsonMapper>(responseBody, HttpStatus.OK));

        // Queue five requests to process them after the fifth
        ResponseEntity<JsonMapper> resp1 = this.shipmentQueue.queue(req1);
        ResponseEntity<JsonMapper> resp2 = this.shipmentQueue.queue(req2);
        ResponseEntity<JsonMapper> resp3 = this.shipmentQueue.queue(req3);
        ResponseEntity<JsonMapper> resp4 = this.shipmentQueue.queue(req4);
        ResponseEntity<JsonMapper> resp5 = this.shipmentQueue.queue(req5);

        // Give time to process response
        try {
            Thread.sleep(1000);
        } 
        catch (InterruptedException e) {
            fail("Test interrupted before finishing.");
        }

        String key1 = this.extractKey(resp1);
        String key2 = this.extractKey(resp2);
        String key3 = this.extractKey(resp3);
        String key4 = this.extractKey(resp4);
        String key5 = this.extractKey(resp5);

        return new String[] { key1, key2, key3, key4, key5 };
    }

    /**
     * Less than five requests should not process
     */
    @Test
    public void testWaitingRequests() {

        String[] keys = this.processThreeRequests();

        assertFalse(this.shipmentQueue.isResponseAvailable(keys[0]));
        assertFalse(this.shipmentQueue.isResponseAvailable(keys[1]));
        assertFalse(this.shipmentQueue.isResponseAvailable(keys[2]));
    }

    /**
     * Helper method to setup three requests in queue and returns the three keys in a processing state.
     */
    private String[] processThreeRequests() {
        
        // Loads of setup
        String req1 = "111";
        String req2 = "222";
        String req3 = "333";

        // Queue five requests to process them after the fifth
        ResponseEntity<JsonMapper> resp1 = this.shipmentQueue.queue(req1);
        ResponseEntity<JsonMapper> resp2 = this.shipmentQueue.queue(req2);
        ResponseEntity<JsonMapper> resp3 = this.shipmentQueue.queue(req3);

        // Give time pause; these should not process regardless
        try {
            Thread.sleep(2000);
        } 
        catch (InterruptedException e) {
            fail("Test interrupted before finishing.");
        }

        String key1 = this.extractKey(resp1);
        String key2 = this.extractKey(resp2);
        String key3 = this.extractKey(resp3);

        return new String[] { key1, key2, key3 };
    }

    private String extractKey(ResponseEntity<JsonMapper> response) {
        Map<String, Object> values = response.getBody().getValues();
        return (String) values.get("key");
    }

    private String constructUrl(String params) {
        return "http://localhost:8080/shipments?q=" + params;
    }
}