package com.kirkchan.poc.xyz;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.kirkchan.poc.xyz.api.queue.JsonMapper;
import com.kirkchan.poc.xyz.api.queue.PricingQueue;
import com.kirkchan.poc.xyz.api.queue.QueueService;
import com.kirkchan.poc.xyz.api.queue.ShipmentQueue;
import com.kirkchan.poc.xyz.api.queue.Status;
import com.kirkchan.poc.xyz.api.queue.TrackQueue;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Tests for the orchestration of queues
 */
@SpringBootTest
class QueueServiceTests {

    @Mock
    ShipmentQueue shipmentQueue;

    @Mock
    TrackQueue trackQueue;

    @Mock
    PricingQueue pricingQueue;

    @InjectMocks
    QueueService queueService;

    /**
     * Test if service accepts request to queue
     */
    @Test
    public void testQueueCalls() {

        ResponseEntity<Status> response = this.performQueueCall();
        assertEquals(HttpStatus.ACCEPTED.value(), response.getStatusCode().value());
        assertEquals("PROCESSING", response.getBody().getStatus());
    }

    /**
     * Test if queue reports processing
     */
    @Test
    public void testQueueStatusProcessing() {

        ResponseEntity<Status> queueResponse = this.performQueueCall();
        // Assumed structure /aggregation/queue/{key}
        String key = queueResponse.getBody().getLocation().split("/")[3];

        ResponseEntity<Status> checkResponse = this.queueService.checkQueue(key);
        assertEquals(HttpStatus.ACCEPTED.value(), checkResponse.getStatusCode().value());
        assertEquals("PROCESSING", checkResponse.getBody().getStatus());
    }

    @Test
    public void testQueueStatusReady() {

        // Five queues should trigger queues to process
        String key = this.setupReadyForKey();

        ResponseEntity<Status> checkResponse = this.queueService.checkQueue(key);
        assertEquals(HttpStatus.SEE_OTHER.value(), checkResponse.getStatusCode().value());
        assertEquals("READY", checkResponse.getBody().getStatus());

    }

    /**
     * Helper to set READY status for a queued request and return key to
     * the request.
     */
    private String setupReadyForKey() {
        ResponseEntity<Status> resp = this.performQueueCall();
        String masterKey = resp.getBody().getLocation().split("/")[3];

        // Master key will map to these queued requests
        Mockito.when(this.shipmentQueue.isResponseAvailable("shipmentKey"))
            .thenReturn(true);
        Mockito.when(this.trackQueue.isResponseAvailable("trackKey"))
            .thenReturn(true);
        Mockito.when(this.pricingQueue.isResponseAvailable("pricingKey"))
            .thenReturn(true);
        
        return masterKey;
    }

    /**
     * Helper to queue predefined requests in each queue.
     */
    private ResponseEntity<Status> performQueueCall() {
        Mockito.when(this.shipmentQueue.queue("shipment"))
            .thenReturn(this.constructResponse("shipmentKey"));
        Mockito.when(this.trackQueue.queue("track"))
            .thenReturn(this.constructResponse("trackKey"));
        Mockito.when(this.pricingQueue.queue("pricing"))
            .thenReturn(this.constructResponse("pricingKey"));

        return this.queueService.queueCalls("shipment", "track", "pricing");
        
    }

    private ResponseEntity<JsonMapper> constructResponse(String key) {
        JsonMapper json = new JsonMapper();
        json.setValue("key", key);

        return new ResponseEntity<JsonMapper>(json, HttpStatus.OK);
    }
}