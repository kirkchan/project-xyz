package com.kirkchan.poc.xyz;

import static org.junit.jupiter.api.Assertions.*;

import com.kirkchan.poc.xyz.api.ApiController;
import com.kirkchan.poc.xyz.api.queue.Aggregate;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class ApiControllerTests {

	@LocalServerPort
	private int port;

	@Autowired
	ApiController controller;

	@Autowired 
	TestRestTemplate apiCaller;

	// @Test
	void contextLoads() {
		assertNotNull(controller);
	}

	// Orders are defined as 9-digits
	@Test
	public void testBadShipmentOrder() {
		// Bad 8-digit order
		String url = formUrl("pricing=NL,CN&track=109347263,123456891&shipments=10934726");
		ResponseEntity<Aggregate> response = apiCaller.getForEntity(url, Aggregate.class);
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCode().value());

		// Bad 10-digit order
		url = formUrl("pricing=NL,CN&track=109347263,123456891&shipments=1093472678");
		response = apiCaller.getForEntity(url, Aggregate.class);
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCode().value());
	}

	// Orders are defined as 9-digits
	@Test
	public void testBadTrackOrder() {
		// Bad 8-digit order
		String url = formUrl("pricing=NL,CN&track=10934726&shipments=109347263,123456891");
		ResponseEntity<Aggregate> response = apiCaller.getForEntity(url, Aggregate.class);
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCode().value());

		// Bad 10-digit order
		url = formUrl("pricing=NL,CN&track==1093472678&shipments=109347263,123456891");
		response = apiCaller.getForEntity(url, Aggregate.class);
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCode().value());
	}

	// ISO codes are defined as ISO-2 country codes
	@Test
	public void getBadCountryCodes() {
		// Bad 1-char country code
		String url = formUrl("pricing=C&track=109347263,123456891&shipments=109347263,123456891");
		ResponseEntity<Aggregate> response = apiCaller.getForEntity(url, Aggregate.class);
		assertEquals(response.getStatusCode().value(), HttpStatus.BAD_REQUEST.value());

		// Bad 3-char country code
		url = formUrl("pricing=CAN,CN&track=109347263,123456891&shipments=109347263,123456891");
		response = apiCaller.getForEntity(url, Aggregate.class);
		assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCode().value());
	}

	private String formUrl(String parameters) {
		return new String(String.format("http://localhost:%s/aggregation?%s", port, parameters));
	}
}
