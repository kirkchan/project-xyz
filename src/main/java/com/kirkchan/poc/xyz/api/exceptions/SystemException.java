package com.kirkchan.poc.xyz.api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
public class SystemException extends RuntimeException {

    private static final long serialVersionUID = -4785649192652356099L;

    public SystemException(String message) {
        super(message);
    }    

}