package com.kirkchan.poc.xyz.api.queue;

/**
 * Response object when a request has been queued
 */
public class Status {
    
    String status;
    String location;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}