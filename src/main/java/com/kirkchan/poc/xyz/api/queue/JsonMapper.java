package com.kirkchan.poc.xyz.api.queue;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;

/**
 * Handler for unknown object names in HTTP responses. 
 * Order numbers, for example, require a JSON mapper for any key value.
 */
public class JsonMapper {
    
    private Map<String, Object> values = new LinkedHashMap<>();

    @JsonAnySetter
    public void setValue(String key, Object value) {
        values.put(key, value);
    }

    @Override
    public String toString() {
        return values.toString();
    }

    public Map<String, Object> getValues() {
        return values;
    }

    public void setValues(Map<String, Object> values) {
        this.values = values;
    }
}