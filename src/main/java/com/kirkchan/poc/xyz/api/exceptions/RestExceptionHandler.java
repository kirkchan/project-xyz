package com.kirkchan.poc.xyz.api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

/**
 * Handler for runtime exceptions. Form them into readable responses.
 */
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<RuntimeErrorResponse> handleBadRequest(Exception ex,
            WebRequest request) {
        
        RuntimeErrorResponse error = new RuntimeErrorResponse();
        error.setTimestamp(LocalDateTime.now());
        error.setError(ex.getLocalizedMessage());
        error.setStatus(HttpStatus.BAD_REQUEST.value());

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<RuntimeErrorResponse> handleNotFound(Exception ex,
            WebRequest request) {

        RuntimeErrorResponse error = new RuntimeErrorResponse();
        error.setTimestamp(LocalDateTime.now());
        error.setError(ex.getLocalizedMessage());
        error.setStatus(HttpStatus.NOT_FOUND.value());

        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(SystemException.class)
    public ResponseEntity<RuntimeErrorResponse> handleSystemError(Exception ex,
            WebRequest request) {

        RuntimeErrorResponse error = new RuntimeErrorResponse();
        error.setTimestamp(LocalDateTime.now());
        error.setError(ex.getLocalizedMessage());
        error.setStatus(HttpStatus.SERVICE_UNAVAILABLE.value());

        return new ResponseEntity<>(error, HttpStatus.SERVICE_UNAVAILABLE);
    }
}