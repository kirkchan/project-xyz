# Change Logs

## 1.2.0

AS-2 completion; schedules added

## 1.1.0

AS-2 completion; queues constructed

## 1.0.2

Queue constructs in endpoint subset .../v2/...

## 1.0.1

First prototype of queue concept

## 1.0.0

AS-1 completion; additional design considerations in TODO comments

## 0.0.2

Integrate backend services

## 0.0.1

Initialize project and high-level technologies
